# Arquitectura Blockchain
Para comenzar a ejecutar los scripts de este repositorio vas a necesitar **NodeJS** y **TypeScript**.  
## Instalando NodeJS
Para instalar NodeJS podes seguir las instrucciones en el sitio oficial
[https://nodejs.org/download](https://nodejs.org/es/download/).
### Comprobar
Para comprobar que la instalación salió bien, podes ejecutar el siguiente comando:
```bash
node -v
```
que debería retornar la versión de NodeJS que tenes instalada. Por ejemplo `v16.13.0`.  

## Instalar TypeScript
```bash
npm i -g typescript ts-node
```
### Comprobar
```bash
ts-node -v
```

## Instalar dependencias del proyecto
Con el siguiente comando vamos a instalar las dependencias basicas de este proyecto.
```bash
npm install
```

## Probar de ejecutar uno de los scripts
A partir de ahora cada uno de los archivos `.ts` los vamos a ejecutar en su mayoría usando el formato
```bash
ts-node Unidad_02/01_hash.ts 
```

## Sobre este repositorio
Vamos a encontrar un directorio/carpeta por unidad, dentro de estas carpetas en generar vamos a a encontrar
un walk-through con código de ejemplo y uno o más archivos terminados en _test.ts que son archivos de test. 
Los archivos te test van a contener una serie de pruebas que seguramente esten fallando, para verificar los test podes
ejecutar el sigueinte comando.
```bash
npm run test
```
### Forma de trabajo
Te vamos a perdir que hagas un fork del repo que vamos a compartir, nos tenes que invitar al fork para que podamos verlo.
En este fork vas a generar un `Merge Request` al branch principal por cada unidad. En el branch que genere dicho `Merge Request` vas a implementar los ejercicios y una vez 
finalizados, vas a compartirnos el link para que hagamos la corrección.